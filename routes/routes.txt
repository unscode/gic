+--------+-----------+-----------------------------------------------+-----------------------------------+---------------------------------------------------------------------------+--------------+
| Domain | Method    | URI                                           | Name                              | Action                                                                    | Middleware   |
+--------+-----------+-----------------------------------------------+-----------------------------------+---------------------------------------------------------------------------+--------------+
|        | GET|HEAD  | /                                             |                                   | Illuminate\Routing\ViewController                                         | web          |
|        | GET|HEAD  | api/game                                      | game.index                        | App\Http\Controllers\Game\GameController@index                            | api,auth:api |
|        | POST      | api/game                                      | game.store                        | App\Http\Controllers\Game\GameController@store                            | api,auth:api |
|        | GET|HEAD  | api/game/{game}                               | game.show                         | App\Http\Controllers\Game\GameController@show                             | api,auth:api |
|        | DELETE    | api/game/{game}                               | game.destroy                      | App\Http\Controllers\Game\GameController@destroy                          | api,auth:api |
|        | PUT|PATCH | api/game/{game}                               | game.update                       | App\Http\Controllers\Game\GameController@update                           | api,auth:api |
|        | POST      | api/game/{game}/medal                         | game.medal.store                  | App\Http\Controllers\Game\Medal\MedalController@store                     | api,auth:api |
|        | GET|HEAD  | api/game/{game}/medal                         | game.medal.index                  | App\Http\Controllers\Game\Medal\MedalController@index                     | api,auth:api |
|        | DELETE    | api/game/{game}/medal/{medal}                 | game.medal.destroy                | App\Http\Controllers\Game\Medal\MedalController@destroy                   | api,auth:api |
|        | PUT|PATCH | api/game/{game}/medal/{medal}                 | game.medal.update                 | App\Http\Controllers\Game\Medal\MedalController@update                    | api,auth:api |
|        | GET|HEAD  | api/game/{game}/phase                         | game.phase.index                  | App\Http\Controllers\Game\Phase\PhaseController@index                     | api,auth:api |
|        | POST      | api/game/{game}/phase                         | game.phase.store                  | App\Http\Controllers\Game\Phase\PhaseController@store                     | api,auth:api |
|        | DELETE    | api/game/{game}/phase/{phase}                 | game.phase.destroy                | App\Http\Controllers\Game\Phase\PhaseController@destroy                   | api,auth:api |
|        | PUT|PATCH | api/game/{game}/phase/{phase}                 | game.phase.update                 | App\Http\Controllers\Game\Phase\PhaseController@update                    | api,auth:api |
|        | POST      | api/game/{game}/player                        | game.player.store                 | App\Http\Controllers\Game\Player\PlayerController@store                   | api,auth:api |
|        | GET|HEAD  | api/game/{game}/player                        | game.player.index                 | App\Http\Controllers\Game\Player\PlayerController@index                   | api,auth:api |
|        | DELETE    | api/game/{game}/player/{player}               | game.player.destroy               | App\Http\Controllers\Game\Player\PlayerController@destroy                 | api,auth:api |
|        | PUT|PATCH | api/game/{game}/player/{player}               | game.player.update                | App\Http\Controllers\Game\Player\PlayerController@update                  | api,auth:api |
|        | POST      | api/game/{game}/player/{player}/medal         | game.player.medal.store           | App\Http\Controllers\Game\Player\Medal\MedalController@store              | api,auth:api |
|        | GET|HEAD  | api/game/{game}/player/{player}/medal         | game.player.medal.index           | App\Http\Controllers\Game\Player\Medal\MedalController@index              | api,auth:api |
|        | DELETE    | api/game/{game}/player/{player}/medal/{medal} | game.player.medal.destroy         | App\Http\Controllers\Game\Player\Medal\MedalController@destroy            | api,auth:api |
|        | GET|HEAD  | api/game/{game}/player/{player}/score         | game.player.score.index           | App\Http\Controllers\Game\Player\Score\ScoreController@index              | api,auth:api |
|        | POST      | api/game/{game}/player/{player}/score         | game.player.score.store           | App\Http\Controllers\Game\Player\Score\ScoreController@store              | api,auth:api |
|        | PUT|PATCH | api/game/{game}/player/{player}/score/{score} | game.player.score.update          | App\Http\Controllers\Game\Player\Score\ScoreController@update             | api,auth:api |
|        | DELETE    | api/game/{game}/player/{player}/score/{score} | game.player.score.destroy         | App\Http\Controllers\Game\Player\Score\ScoreController@destroy            | api,auth:api |
|        | POST      | api/game/{game}/score                         | game.score.store                  | App\Http\Controllers\Game\Score\ScoreController@store                     | api,auth:api |
|        | GET|HEAD  | api/game/{game}/score                         | game.score.index                  | App\Http\Controllers\Game\Score\ScoreController@index                     | api,auth:api |
|        | PUT|PATCH | api/game/{game}/score/{score}                 | game.score.update                 | App\Http\Controllers\Game\Score\ScoreController@update                    | api,auth:api |
|        | DELETE    | api/game/{game}/score/{score}                 | game.score.destroy                | App\Http\Controllers\Game\Score\ScoreController@destroy                   | api,auth:api |
|        | GET|HEAD  | api/medal                                     | medal.index                       | App\Http\Controllers\Medal\MedalController@index                          | api,auth:api |
|        | POST      | api/medal                                     | medal.store                       | App\Http\Controllers\Medal\MedalController@store                          | api,auth:api |
|        | GET|HEAD  | api/medal/search/{search}                     |                                   | App\Http\Controllers\Medal\MedalController@search                         | api,auth:api |
|        | PUT|PATCH | api/medal/{medal}                             | medal.update                      | App\Http\Controllers\Medal\MedalController@update                         | api,auth:api |
|        | GET|HEAD  | api/medal/{medal}                             | medal.show                        | App\Http\Controllers\Medal\MedalController@show                           | api,auth:api |
|        | DELETE    | api/medal/{medal}                             | medal.destroy                     | App\Http\Controllers\Medal\MedalController@destroy                        | api,auth:api |
|        | POST      | api/user                                      | user.store                        | App\Http\Controllers\User\UserController@store                            | api,auth:api |
|        | GET|HEAD  | api/user                                      | user.index                        | App\Http\Controllers\User\UserController@index                            | api,auth:api |
|        | GET|HEAD  | api/user/player/search/{search}               |                                   | App\Http\Controllers\User\Actor\PlayerController@search                   | api,auth:api |
|        | DELETE    | api/user/{user}                               | user.destroy                      | App\Http\Controllers\User\UserController@destroy                          | api,auth:api |
|        | PUT|PATCH | api/user/{user}                               | user.update                       | App\Http\Controllers\User\UserController@update                           | api,auth:api |
|        | GET|HEAD  | home                                          | home                              | App\Http\Controllers\HomeController@index                                 | web,auth     |
|        | POST      | login                                         |                                   | App\Http\Controllers\Auth\LoginController@login                           | web,guest    |
|        | GET|HEAD  | login                                         | login                             | App\Http\Controllers\Auth\LoginController@showLoginForm                   | web,guest    |
|        | POST      | logout                                        | logout                            | App\Http\Controllers\Auth\LoginController@logout                          | web          |
|        | GET|HEAD  | oauth/authorize                               | passport.authorizations.authorize | Laravel\Passport\Http\Controllers\AuthorizationController@authorize       | web,auth     |
|        | DELETE    | oauth/authorize                               | passport.authorizations.deny      | Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny        | web,auth     |
|        | POST      | oauth/authorize                               | passport.authorizations.approve   | Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve  | web,auth     |
|        | POST      | oauth/clients                                 | passport.clients.store            | Laravel\Passport\Http\Controllers\ClientController@store                  | web,auth     |
|        | GET|HEAD  | oauth/clients                                 | passport.clients.index            | Laravel\Passport\Http\Controllers\ClientController@forUser                | web,auth     |
|        | DELETE    | oauth/clients/{client_id}                     | passport.clients.destroy          | Laravel\Passport\Http\Controllers\ClientController@destroy                | web,auth     |
|        | PUT       | oauth/clients/{client_id}                     | passport.clients.update           | Laravel\Passport\Http\Controllers\ClientController@update                 | web,auth     |
|        | POST      | oauth/personal-access-tokens                  | passport.personal.tokens.store    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store     | web,auth     |
|        | GET|HEAD  | oauth/personal-access-tokens                  | passport.personal.tokens.index    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser   | web,auth     |
|        | DELETE    | oauth/personal-access-tokens/{token_id}       | passport.personal.tokens.destroy  | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy   | web,auth     |
|        | GET|HEAD  | oauth/scopes                                  | passport.scopes.index             | Laravel\Passport\Http\Controllers\ScopeController@all                     | web,auth     |
|        | POST      | oauth/token                                   | passport.token                    | Laravel\Passport\Http\Controllers\AccessTokenController@issueToken        | throttle     |
|        | POST      | oauth/token/refresh                           | passport.token.refresh            | Laravel\Passport\Http\Controllers\TransientTokenController@refresh        | web,auth     |
|        | GET|HEAD  | oauth/tokens                                  | passport.tokens.index             | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser | web,auth     |
|        | DELETE    | oauth/tokens/{token_id}                       | passport.tokens.destroy           | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy | web,auth     |
|        | POST      | password/email                                | password.email                    | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail     | web,guest    |
|        | GET|HEAD  | password/reset                                | password.request                  | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm    | web,guest    |
|        | POST      | password/reset                                | password.update                   | App\Http\Controllers\Auth\ResetPasswordController@reset                   | web,guest    |
|        | GET|HEAD  | password/reset/{token}                        | password.reset                    | App\Http\Controllers\Auth\ResetPasswordController@showResetForm           | web,guest    |
|        | GET|HEAD  | register                                      | register                          | App\Http\Controllers\Auth\RegisterController@showRegistrationForm         | web,guest    |
|        | POST      | register                                      |                                   | App\Http\Controllers\Auth\RegisterController@register                     | web,guest    |
|        | GET|HEAD  | test                                          | test                              | App\Http\Controllers\Test\TestController@index                            | web          |
|        | GET|HEAD  | user                                          | user.index                        | App\Http\Controllers\Administration\User\UserController@index             | web,auth     |
|        | PUT       | user/{user}/actor/{actor}                     | user.actor.update                 | App\Http\Controllers\Administration\User\Actor\ActorController@update     | web,auth     |
+--------+-----------+-----------------------------------------------+-----------------------------------+---------------------------------------------------------------------------+--------------+
